from src import archive


while True: #Выполонение цикла истинности
    try: #Проверка введённых значений
        a = float(input('Введите значение а: ')) #Ввод а - постоянного символа
        lower = float(input('Введите нижний предел интегрирования: ')) #Ввод нижней границы х
        upper = float(input('Введите верхний предел интегрирования: ')) #Ввод верхней границы х
    except ValueError:
        print('Ошибка ввода')

    try: #Проверка на ошибку в случае, если введённые значения выдадут ошибку в реализованных функциях
        if lower < upper: #Если нижняя граница меньше чем верхняя, выводятся данные
            print(f'{archive.simpson(archive.G, a, lower, upper):.6f}, Вычисление методом Симпсона')
            print(f'{archive.rectangle(archive.G, a, lower, upper):.6f}, Вычисление методом прямоугольника')
            print(f'{archive.trapezoid(archive.G, a, lower, upper):.6f}, Вычисление методом трапеции')
    except ValueError:
        print('Ошибка ввода')

    exit_choice = input("Начать выполнение программы снова? - (Д/Н)") #Код выхода/повтора программы
    if 'YyNnДдНн'.find(exit_choice) == -1:
        print('Ошибка выбора.')
    elif 'YyДд'.find(exit_choice) >= 0:
        pass
    elif 'NnНн'.find(exit_choice) >= 0:
        raise SystemExit()


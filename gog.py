def rect_integral(g,a,xmin,xmax,n):
    dx=(xmax-xmin)/n
    area=0
    x=xmin
    for i in range(n):
        area+=dx*g(a,x)
        x+=dx
    return area

def G(a, x): #Функция из первой лабораторной работы первого семестра
    func = (6 * (4 * a ** 2 - 12 * a * x + 5 * x ** 2)) / (9 * a ** 2 + 30 * a * x + 16 * x ** 2)
    return func
print(rect_integral(G, 3, 5, 10, 8))
#0.640177